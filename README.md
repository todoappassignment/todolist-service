# TodoList Service
TodoList Backend Service

| Project link:| [http://35.228.218.79/todos](http://35.228.218.79/todos) |
| -----------  | ----------- |

## Requirements
- [Java11](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven](https://maven.apache.org/)
- [Docker](https://www.docker.com/)

## Features
* Get list of todos
* Update todo
* Delete todo

## Tech Stack
* Java: 11
* Spring Boot
* Spring Data Mongodb
* Lombok
* Spring Cloud Contract
* Testcontainers

## Project Structure

```
.        
├── src           
|   ├── main                  # Implementation Folder/Package
|   |   ├── java
|   |   ├── resources         # Application Configs and Error Message Sources
|   ├── test                  # Tests Folger/Package
|   |   ├── java              # Unit & Integration Tests
|   |   ├── resources
|   |   |   ├── contracts     # Contract Tests
├── .gitlab-ci.yml            # Pipeline file
├── Dockerfile
├── k8s                       # Kubernetes config files
├── README.md
├── pom.xml
```
## Build and Tests
### Building the project

Build Project
```
$ mvn install
```

### Running tests 

Run unit/integration/contract tests
```
$ mvn test
```

## Deploy Project 
[Gitlab CI/CD pipeline](.gitlab-ci.yml) is defined with 4 stage. (build, test, dockerize, deploy) And triggers acceptance tests after deploy stage