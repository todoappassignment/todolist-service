package io.github.demirmustafa.todolistservice.service;

import io.github.demirmustafa.todolistservice.common.exception.EntityNotFoundException;
import io.github.demirmustafa.todolistservice.converter.todo.response.TodoResponseConverter;
import io.github.demirmustafa.todolistservice.domain.entity.Todo;
import io.github.demirmustafa.todolistservice.domain.repository.TodoRepository;
import io.github.demirmustafa.todolistservice.model.todo.request.CreateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.request.UpdateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.response.TodoResource;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TodoService {

    private final TodoRepository todoRepository;
    private final TodoResponseConverter todoResponseConverter;

    public List<TodoResource> getAll() {
        return todoRepository.findAll()
                .stream()
                .map(todoResponseConverter::convertTodoToResource)
                .collect(Collectors.toList());
    }

    public TodoResource create(CreateTodoRequest request) {
        Todo todo = Todo.builder()
                .title(request.getTitle())
                .build();

        Todo saved = todoRepository.save(todo);
        return todoResponseConverter.convertTodoToResource(saved);
    }

    public void completeTodo(String id) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        if (todoOptional.isPresent()) {
            Todo todo = todoOptional.get();
            todo.setCompleted(Boolean.TRUE);
            todoRepository.save(todo);
        }
    }

    public void inCompleteTodo(String id) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        if (todoOptional.isPresent()) {
            Todo todo = todoOptional.get();
            todo.setCompleted(Boolean.FALSE);
            todoRepository.save(todo);
        }
    }

    public void delete(String id) {
        Optional<Todo> todoOptional = todoRepository.findById(id);
        todoOptional.ifPresent(todoRepository::delete);
    }

    public TodoResource update(String id, UpdateTodoRequest request) {
        Todo todo = todoRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("errors.todo.notFound"));
        todo.setCompleted(request.getCompleted());
        Todo updated = todoRepository.save(todo);
        return todoResponseConverter.convertTodoToResource(updated);
    }
}
