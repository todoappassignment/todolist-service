package io.github.demirmustafa.todolistservice.domain.repository;

import io.github.demirmustafa.todolistservice.domain.entity.Todo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TodoRepository extends MongoRepository<Todo, String> {
}
