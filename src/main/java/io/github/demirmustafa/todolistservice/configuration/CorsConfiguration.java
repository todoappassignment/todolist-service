package io.github.demirmustafa.todolistservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfiguration {
    @Bean
    public WebMvcConfigurer webMvcConfigurer() {
        return new WebMvcConfigurer() {

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("PUT", "DELETE", "POST", "GET", "OPTIONS", "PATCH")
                        //.allowedHeaders("header1", "header2", "header3")
                        //.exposedHeaders("header1", "header2")
                        .allowCredentials(false)
                        .maxAge(1800);
            }
        };
    }
}
