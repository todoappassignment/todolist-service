package io.github.demirmustafa.todolistservice.controller.advice;

import io.github.demirmustafa.todolistservice.common.exception.EntityNotFoundException;
import io.github.demirmustafa.todolistservice.model.error.ErrorResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ValidationException;
import java.util.Locale;

@RestControllerAdvice
@RequiredArgsConstructor
public class TodoControllerAdvice {

    private final MessageSource messageSource;

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleTodoListException(EntityNotFoundException exception) {
        return ErrorResponse.builder()
                .status(exception.getStatus())
                .message(messageSource.getMessage(exception.getMessageCode(), null, Locale.ENGLISH))
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handle(MethodArgumentNotValidException exception){
        FieldError fieldError = exception.getBindingResult().getFieldError();
        return ErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(messageSource.getMessage(fieldError.getDefaultMessage(), null, Locale.ENGLISH))
                .build();
    }
}
