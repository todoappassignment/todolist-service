package io.github.demirmustafa.todolistservice.common.exception;

public class EntityNotFoundException extends TodoListException {

    public EntityNotFoundException(String messageCode) {
        super(404, messageCode);
    }
}
