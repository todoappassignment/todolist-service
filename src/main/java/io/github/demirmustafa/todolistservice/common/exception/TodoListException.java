package io.github.demirmustafa.todolistservice.common.exception;

import lombok.Getter;

@Getter
public class TodoListException extends RuntimeException {

    private Integer status;
    private String messageCode;

    public TodoListException(Integer status, String messageCode) {
        super(messageCode);
        this.status = status;
        this.messageCode = messageCode;
    }
}
