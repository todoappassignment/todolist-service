package io.github.demirmustafa.todolistservice.model.todo.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TodoResource {

    private String id;
    private String title;
    private Boolean completed;
}
