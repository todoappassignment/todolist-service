package io.github.demirmustafa.todolistservice.model.todo.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreateTodoRequest {

    @NotNull(message = "error.todo.createRequest.title.required")
    private String title;
}
