package io.github.demirmustafa.todolistservice.model.todo.request;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UpdateTodoRequest {

    @NotNull(message = "error.todo.updateRequest.title.required")
    private String title;
    @NotNull(message = "error.todo.updateRequest.completed.required")
    private Boolean completed;
}
