package io.github.demirmustafa.todolistservice.converter.todo.response;

import io.github.demirmustafa.todolistservice.domain.entity.Todo;
import io.github.demirmustafa.todolistservice.model.todo.response.TodoResource;
import org.springframework.stereotype.Component;

@Component
public class TodoResponseConverter {

    public TodoResource convertTodoToResource(Todo todo) {
        return TodoResource.builder()
                .id(todo.getId())
                .title(todo.getTitle())
                .completed(todo.getCompleted())
                .build();
    }
}
