import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Create todo"
    request {
        method POST()
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        url('/todos')
        body(file("CREATE_TODO_REQUEST.json"))
    }
    response {
        status CREATED()
        body(file("CREATE_TODO_RESPONSE.json"))
    }
}

Contract.make {
    description "Returns ErrorResponse when request.title is empty"
    request {
        method POST()
        url('/todos')
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        body(
                '''
            {}
        '''
        )
    }

    response {
        status BAD_REQUEST()
        body(
                '''
            {
                "status": 400,
                "message": "Title attribute must be provided to create todo!"
            }
            '''
        )
    }
}