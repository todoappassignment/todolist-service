import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Update todo completion as completed"
    request {
        method PUT()
        url('/todos/12345')
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        body '''
            {
                "title": "buy some milk",
                "completed": true
            }
        '''
    }

    response {
        status ACCEPTED()
        body '''
            {
                "id": "12345",
                "title": "buy some milk",
                "completed": true
            }
        '''
    }
}

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Update todo completion as in completed"
    request {
        method PUT()
        url('/todos/12345')
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        body '''
            {
                "title": "buy some milk",
                "completed": false
            }
        '''
    }

    response {
        status ACCEPTED()
        body '''
            {
                "id": "12345",
                "title": "buy some milk",
                "completed": false
            }
        '''
    }
}

Contract.make {
    description "Returns ErrorResponse when request.title is empty"
    request {
        method PUT()
        url('/todos/12345')
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        body(
                '''
            {
                "completed": false
            }
        '''
        )
    }

    response {
        status BAD_REQUEST()
        body(
                '''
            {
                "status": 400,
                "message": "Title attribute must be provided to update todo!"
            }
            '''
        )
    }
}

Contract.make {
    description "Returns ErrorResponse when request.completed is empty"
    request {
        method PUT()
        url('/todos/12345')
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        body(
                '''
            {
                "title": "buy some milk"
            }
        '''
        )
    }

    response {
        status BAD_REQUEST()
        body(
                '''
            {
                "status": 400,
                "message": "Completed attribute must be provided to update todo!"
            }
            '''
        )
    }
}

Contract.make {
    description "Returns ErrorResponse when no todo found to update"
    request {
        method PUT()
        url('/todos/unExistTodoId')
        headers {
            accept(applicationJson())
            contentType(applicationJson())
        }
        body(
                '''
            {
                "title": "un exist todo",
                "completed": false
            }
        '''
        )
    }
    response {
        status NOT_FOUND()
        body(
                '''
            {
                "status": 404,
                "message": "No todo found by the specified id!"
            }
        '''
        )
    }
}