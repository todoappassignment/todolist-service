import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Get todos"
    request {
        method GET()
        headers {
            accept(applicationJson())
        }
        url('/todos')
    }
    response {
        status OK()
        headers {
            contentType(applicationJson())
        }
        body(file("TODO_LIST_RESPONSE.json"))
    }
}