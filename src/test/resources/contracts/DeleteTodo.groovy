import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "Delete todo"
    request {
        method DELETE()
        url('/todos/12345')
    }
    response {
        status NO_CONTENT()
    }
}