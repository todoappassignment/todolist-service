package io.github.demirmustafa.todolistservice.service;

import io.github.demirmustafa.todolistservice.common.exception.EntityNotFoundException;
import io.github.demirmustafa.todolistservice.converter.todo.response.TodoResponseConverter;
import io.github.demirmustafa.todolistservice.domain.entity.Todo;
import io.github.demirmustafa.todolistservice.domain.repository.TodoRepository;
import io.github.demirmustafa.todolistservice.model.todo.request.CreateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.request.UpdateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.response.TodoResource;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class TodoServiceTest {

    @Mock
    private TodoRepository todoRepository;

    @Mock
    private TodoResponseConverter todoResponseConverter;

    @InjectMocks
    private TodoService todoService;

    @Captor
    private ArgumentCaptor<Todo> todoArgumentCaptor;

    @After
    public void tearDown() {
        verifyNoMoreInteractions(todoRepository, todoResponseConverter);
    }

    @Test
    public void should_get_all_todos() {
        //given
        List<Todo> todos = Collections.singletonList(
                Todo.builder()
                        .id("12345")
                        .title("buy some milk")
                        .completed(true)
                        .build()
        );

        TodoResource resource = TodoResource.builder()
                .id("12345")
                .title("buy some milk")
                .completed(true).build();

        doReturn(todos).when(todoRepository).findAll();
        doReturn(resource).when(todoResponseConverter).convertTodoToResource(any(Todo.class));

        //when
        List<TodoResource> all = todoService.getAll();

        //then
        assertNotNull(all);
        assertEquals(1, all.size());
        assertEquals("12345", all.get(0).getId());
        assertEquals("buy some milk", all.get(0).getTitle());
        assertTrue(all.get(0).getCompleted());

        verify(todoRepository).findAll();
        verify(todoResponseConverter).convertTodoToResource(todoArgumentCaptor.capture());
        assertNotNull(todoArgumentCaptor);
        assertEquals("12345", todoArgumentCaptor.getValue().getId());
        assertEquals("buy some milk", todoArgumentCaptor.getValue().getTitle());
        assertTrue(todoArgumentCaptor.getValue().getCompleted());
    }

    @Test
    public void should_create_todo() {
        //given
        CreateTodoRequest request = new CreateTodoRequest("buy some milk");

        Todo saved = Todo.builder()
                .id("12345")
                .title("buy some milk")
                .completed(false)
                .build();

        TodoResource expected = TodoResource.builder()
                .id("12345")
                .title("buy some milk")
                .completed(false)
                .build();

        doReturn(saved).when(todoRepository).save(any(Todo.class));
        doReturn(expected).when(todoResponseConverter).convertTodoToResource(any(Todo.class));

        //when
        TodoResource resource = todoService.create(request);

        //then
        assertNotNull(resource);
        assertEquals("12345", resource.getId());
        assertEquals("buy some milk", resource.getTitle());
        assertFalse(resource.getCompleted());

        verify(todoRepository).save(todoArgumentCaptor.capture());
        assertNull(todoArgumentCaptor.getValue().getId());
        assertEquals("buy some milk", todoArgumentCaptor.getValue().getTitle());
        assertFalse(todoArgumentCaptor.getValue().getCompleted());

        verify(todoResponseConverter).convertTodoToResource(todoArgumentCaptor.capture());
        assertEquals("12345", todoArgumentCaptor.getValue().getId());
        assertEquals("buy some milk", todoArgumentCaptor.getValue().getTitle());
        assertFalse(todoArgumentCaptor.getValue().getCompleted());
    }

    @Test
    public void should_update_todo_as_completed() {
        //given
        UpdateTodoRequest request = new UpdateTodoRequest("buy some milk", true);
        Todo todo = Todo.builder()
                .id("12345")
                .title("buy some milk")
                .completed(false)
                .build();
        TodoResource resource = TodoResource.builder()
                .id("12345")
                .title("buy some milk")
                .completed(true)
                .build();

        doReturn(Optional.of(todo)).when(todoRepository).findById(anyString());
        doReturn(resource).when(todoResponseConverter).convertTodoToResource(any(Todo.class));
        doReturn(new Todo("12345", "buy some milk", true)).when(todoRepository).save(any(Todo.class));


        //when
        TodoResource updated = todoService.update("12345", request);

        //then
        assertNotNull(updated);
        assertEquals("12345", updated.getId());
        assertEquals(true, updated.getCompleted());
        verify(todoRepository).findById(anyString());
        verify(todoRepository).save(todoArgumentCaptor.capture());
        assertEquals("12345", todoArgumentCaptor.getValue().getId());
        assertTrue(todoArgumentCaptor.getValue().getCompleted());
        verify(todoResponseConverter).convertTodoToResource(todoArgumentCaptor.capture());
        assertTrue(todoArgumentCaptor.getValue().getCompleted());
    }

    @Test
    public void should_update_todo_as_in_completed() {
        //given
        UpdateTodoRequest request = new UpdateTodoRequest("buy some milk", false);
        Todo todo = Todo.builder()
                .id("12345")
                .title("buy some milk")
                .completed(true)
                .build();
        TodoResource resource = TodoResource.builder()
                .id("12345")
                .title("buy some milk")
                .completed(false)
                .build();

        doReturn(Optional.of(todo)).when(todoRepository).findById(anyString());
        doReturn(resource).when(todoResponseConverter).convertTodoToResource(any(Todo.class));
        doReturn(new Todo("12345", "buy some milk", false)).when(todoRepository).save(any(Todo.class));


        //when
        TodoResource updated = todoService.update("12345", request);

        //then
        assertNotNull(updated);
        assertEquals("12345", updated.getId());
        assertEquals(false, updated.getCompleted());
        verify(todoRepository).findById(anyString());
        verify(todoRepository).save(todoArgumentCaptor.capture());
        assertEquals("12345", todoArgumentCaptor.getValue().getId());
        assertFalse(todoArgumentCaptor.getValue().getCompleted());
        verify(todoResponseConverter).convertTodoToResource(todoArgumentCaptor.capture());
        assertFalse(todoArgumentCaptor.getValue().getCompleted());
    }

    @Test
    public void should_throw_EntityNotFoundException_when_no_todo_found_to_update_by_id() {
        //given
        doReturn(Optional.empty()).when(todoRepository).findById(anyString());

        try {
            //when
            todoService.update("testId", new UpdateTodoRequest());
        } catch (EntityNotFoundException e) {
            //then
            verify(todoRepository).findById(anyString());
        }
    }

    @Test
    public void should_delete_todo() {
        //given
        Todo todo = Todo.builder()
                .id("12345")
                .title("buy some milk")
                .completed(false)
                .build();

        doReturn(Optional.of(todo)).when(todoRepository).findById(anyString());

        //when
        todoService.delete("12345");

        //then
        verify(todoRepository).findById(anyString());
        verify(todoRepository).delete(todoArgumentCaptor.capture());
        assertNotNull(todoArgumentCaptor.getValue());
        assertEquals("12345", todoArgumentCaptor.getValue().getId());
    }
}