package io.github.demirmustafa.todolistservice;

import io.github.demirmustafa.todolistservice.common.exception.EntityNotFoundException;
import io.github.demirmustafa.todolistservice.model.todo.request.CreateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.request.UpdateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.response.TodoResource;
import io.github.demirmustafa.todolistservice.service.TodoService;
import io.restassured.RestAssured;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class TodolistServiceApplicationTests {

    @Autowired
    private WebApplicationContext context;

    @LocalServerPort
    private Integer port;

    @MockBean
    private TodoService todoService;

    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;

        DefaultMockMvcBuilder defaultMockMvcBuilder = MockMvcBuilders.webAppContextSetup(context);
        RestAssuredMockMvc.standaloneSetup(defaultMockMvcBuilder);

        mockGetAllTodosResponse();
        mockCreateTodoResponse();
        mockCompleteTodo();
        mockInCompleteTodo();
        mockUnExistTodoUpdate();
    }

    @Test
    void contextLoads() {
        assertThat(1L, equalTo(1L));
    }


    private void mockGetAllTodosResponse() {
        //get all todos
        doReturn(Collections.singletonList(
                TodoResource.builder()
                        .id("12345")
                        .title("buy some milk")
                        .completed(false)
                        .build()
        )).when(todoService).getAll();
    }

    private void mockCreateTodoResponse() {
        //create _todo
        doReturn(
                TodoResource.builder()
                        .id("12345")
                        .title("buy some milk")
                        .completed(false)
                        .build()
        ).when(todoService).create(any(CreateTodoRequest.class));
    }

    private void mockCompleteTodo() {
        doReturn(TodoResource.builder()
                .id("12345")
                .title("buy some milk")
                .completed(true).build())
                .when(todoService)
                .update(eq("12345"), eq(new UpdateTodoRequest("buy some milk", true)));
    }

    private void mockInCompleteTodo() {
        doReturn(TodoResource.builder()
                .id("12345")
                .title("buy some milk")
                .completed(false).build())
                .when(todoService)
                .update(eq("12345"), eq(new UpdateTodoRequest("buy some milk", false)));
    }

    private void mockUnExistTodoUpdate() {
        doThrow(new EntityNotFoundException("errors.todo.notFound")).when(todoService).update(eq("unExistTodoId"), any(UpdateTodoRequest.class));
    }

}
