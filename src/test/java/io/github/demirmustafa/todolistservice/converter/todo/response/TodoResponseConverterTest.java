package io.github.demirmustafa.todolistservice.converter.todo.response;

import io.github.demirmustafa.todolistservice.domain.entity.Todo;
import io.github.demirmustafa.todolistservice.model.todo.response.TodoResource;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class TodoResponseConverterTest {

    private TodoResponseConverter converter;

    @Before
    public void setup() {
        converter = new TodoResponseConverter();
    }

    @Test
    public void should_convert_todo_to_resource() {
        //given
        Todo todo = new Todo("12345", "buy some milk", false);

        //when
        TodoResource resource = converter.convertTodoToResource(todo);

        //then
        assertNotNull(resource);
        assertEquals("12345", resource.getId());
        assertEquals("buy some milk", resource.getTitle());
        assertFalse(resource.getCompleted());
    }
}