package io.github.demirmustafa.todolistservice.integration;

import io.github.demirmustafa.todolistservice.domain.entity.Todo;
import io.github.demirmustafa.todolistservice.model.error.ErrorResponse;
import io.github.demirmustafa.todolistservice.model.todo.request.CreateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.request.UpdateTodoRequest;
import io.github.demirmustafa.todolistservice.model.todo.response.TodoResource;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(SpringExtension.class)
public class TodolistIntegrationTest extends BaseIntegrationTest {

    private TestRestTemplate testRestTemplate = new TestRestTemplate();

    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setup() {
        mongoTemplate.dropCollection(Todo.class);
    }


    @Test
    public void should_get_empty_todo_list() {
        //given

        //when
        TodoResource[] todos = testRestTemplate.getForObject(getBaseUrl(), TodoResource[].class);

        //then
        assertNotNull(todos);
        assertEquals(0, todos.length);
    }

    @Test
    @SneakyThrows
    public void should_get_todo_list() {
        //given
        Todo todo = new Todo(null, "buy some milk", false);
        Todo saved = mongoTemplate.save(todo);

        //when
        TodoResource[] todos = testRestTemplate.getForObject(getBaseUrl(), TodoResource[].class);

        //then
        assertNotNull(todos);
        assertEquals(1, todos.length);
        assertEquals(saved.getId(), todos[0].getId());
        assertEquals(saved.getTitle(), todos[0].getTitle());
        assertEquals(saved.getCompleted(), todos[0].getCompleted());
    }

    @Test
    public void should_create_todo() {
        //given
        CreateTodoRequest request = new CreateTodoRequest("buy some milk");

        //when
        TodoResource todo = testRestTemplate.postForObject(getBaseUrl(), request, TodoResource.class);

        //then
        assertNotNull(todo);
        assertNotNull(todo.getId());
        assertEquals("buy some milk", todo.getTitle());
        assertFalse(todo.getCompleted());
    }

    @Test
    public void should_return_BadRequest_error_response_when_create_request_title_attribute_is_empty() {
        //given
        CreateTodoRequest request = new CreateTodoRequest();

        //when
        ErrorResponse errorResponse = testRestTemplate.postForObject(getBaseUrl(), request, ErrorResponse.class);

        //then
        assertNotNull(errorResponse);
        assertEquals(HttpStatus.BAD_REQUEST.value(), errorResponse.getStatus().intValue());
        assertEquals("Title attribute must be provided to create todo!", errorResponse.getMessage());
    }

    @Test
    public void should_complete_todo() {
        //given
        Todo todo = Todo.builder()
                .title("buy some milk")
                .build();
        Todo saved = mongoTemplate.save(todo);

        UpdateTodoRequest request = new UpdateTodoRequest("buy some milk", true);
        HttpEntity entity = new HttpEntity(request);

        //when
        ResponseEntity<TodoResource> response = testRestTemplate.exchange(getBaseUrl() + "/" + saved.getId(), HttpMethod.PUT, entity, TodoResource.class);

        //then
        assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
        assertEquals(saved.getId(), response.getBody().getId());
        assertEquals("buy some milk", response.getBody().getTitle());
        assertTrue(response.getBody().getCompleted());
    }

    @Test
    public void should_in_complete_todo() {
        //given
        Todo todo = Todo.builder()
                .title("buy some milk")
                .completed(true)
                .build();
        Todo saved = mongoTemplate.save(todo);

        UpdateTodoRequest request = new UpdateTodoRequest("buy some milk", false);
        HttpEntity entity = new HttpEntity(request);

        //when
        ResponseEntity<TodoResource> response = testRestTemplate.exchange(getBaseUrl() + "/" + saved.getId(), HttpMethod.PUT, entity, TodoResource.class);

        //then
        assertEquals(HttpStatus.ACCEPTED, response.getStatusCode());
        assertEquals(saved.getId(), response.getBody().getId());
        assertEquals("buy some milk", response.getBody().getTitle());
        assertFalse(response.getBody().getCompleted());
    }

    @Test
    public void should_return_BadRequest_error_response_when_update_request_title_attribute_is_empty(){
        //given
        HttpEntity<UpdateTodoRequest> entity = new HttpEntity<>(new UpdateTodoRequest(null, false));

        //when
        ResponseEntity<ErrorResponse> exchange = testRestTemplate.exchange(getBaseUrl() + "/12345", HttpMethod.PUT, entity, ErrorResponse.class);

        //then
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());
        assertEquals(HttpStatus.BAD_REQUEST.value(), exchange.getBody().getStatus().intValue());
        assertEquals("Title attribute must be provided to update todo!", exchange.getBody().getMessage());
    }

    @Test
    public void should_return_BadRequest_error_response_when_update_request_completed_attribute_is_empty(){
        //given
        HttpEntity<UpdateTodoRequest> entity = new HttpEntity<>(new UpdateTodoRequest("buy some milk", null));

        //when
        ResponseEntity<ErrorResponse> exchange = testRestTemplate.exchange(getBaseUrl() + "/12345", HttpMethod.PUT, entity, ErrorResponse.class);

        //then
        assertEquals(HttpStatus.BAD_REQUEST, exchange.getStatusCode());
        assertEquals(HttpStatus.BAD_REQUEST.value(), exchange.getBody().getStatus().intValue());
        assertEquals("Completed attribute must be provided to update todo!", exchange.getBody().getMessage());
    }

    @Test
    public void should_delete_todo() {
        //given
        Todo todo = Todo.builder()
                .title("buy some milk")
                .build();
        Todo saved = mongoTemplate.save(todo);

        //when
        testRestTemplate.delete(getBaseUrl() + "/" + saved.getId());

        //then
        Query query = new Query().addCriteria(Criteria.where("id").is(todo.getId()));
        Todo deleted = mongoTemplate.findOne(query, Todo.class);
        assertNull(deleted);
    }

    private String getBaseUrl() {
        return "http://localhost:" + getPort() + "/todos";
    }
}
