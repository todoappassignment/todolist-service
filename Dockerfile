FROM maven:3.6.3-jdk-11 AS build-env
ADD . ./todoapp
WORKDIR /todoapp
RUN mvn clean install -DskipTests

FROM openjdk:11-jre-slim
COPY --from=build-env ./todoapp/target/todolist-service-0.0.1-SNAPSHOT.jar app.jar
CMD java -jar app.jar

HEALTHCHECK --interval=3s --timeout=3s --retries=30 \
  CMD curl -f http://localhost:8080/actuator/health || exit 1